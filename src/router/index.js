import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/nishikata',
    name: 'nishikata',
    component: () => import('../views/NishikataView.vue')
  },
  {
    path: '/mori',
    name: 'mori',
    component: () => import('../views/MoriView.vue')
  },
  {
    path: '/sera',
    name: 'sera',
    component: () => import('../views/SeraView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
